package mystore;

// Maximus Curkovic, 101139937
// Christopher Semaan, 101140813

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class for the Inventory of the store.
 *
 * @author Maximus Curkovic, 101139937
 * @author Christopher Semaan, 101140813
 */
public class Inventory implements ProductStockContainer {

    private HashMap<Product, Integer> Products;

    /**
     * Default constructor for class Inventory.
     */
    public Inventory() {
        this.Products = new HashMap<Product, Integer>();
    }

    /**
     * Getter method for returning the hashmap of products in the inventory of the store.
     * @return A Product object, Integer HashMap Products.
     */
    public HashMap<Product, Integer> getProducts() {
        return Products;
    }

    /**
     * Getter method for getting the size of the Products hashmap.
     * @return An integer size of Products.
     */
    @Override
    public int getNumOfProducts() {
        return Products.size();
    }

    /**
     * Setter method for setting the Products in the inventory.
     * @param products A Product object, Integer HashMap products.
     */
    public void setProducts(HashMap<Product, Integer> products) {
        Products = products;
    }

    public ArrayList<Product> getProductsAsArrayList(){
        return new ArrayList<Product>(this.Products.keySet());
    }
    /**
     * Getter method for getting the stock of the inventory.
     * @param product A Product object product.
     * @return The stock of the product in the inventory. Will return 0 if the product does not exist.
     */
    public int getProductQuantity(Product product) {
        for (Product i : this.Products.keySet()) {
            if (i.getId() == product.getId()) {
                return this.Products.get(i);
            }
        }
        return 0;
    }

    /**
     * Method for adding stock to the product in the inventory.
     * @param product A Product object product.
     * @param stock An integer stock value.
     */
    public void addProductQuantity(Product product, int stock) {

        if (this.Products.containsKey(product)){
            for (Product i: this.Products.keySet()) {
                if (product == i) {
                    this.Products.replace(product, this.Products.get(product),this.Products.get(product) + stock);
                }
            }
        }
        else {
            this.Products.put(product, stock);
        }
    }

    /**
     * Method for removing stock from the product in the inventory, using the product ID.
     * @param product A Product object product.
     * @param stock An integer stock value.
     */
    public void removeProductQuantity(Product product, int stock) {

        for (Product i : this.Products.keySet()) {
            if (i.getId() == product.getId()) {
                this.Products.replace(i, this.Products.get(i),this.Products.get(i) - stock);
                if (this.Products.get(i) < 0) {
                    this.Products.replace(i, 0);
                }
            }
        }
    }

    /**
     * Method for getting the product name from the product ID.
     * @param productID An integer product ID.
     * @return The product name associated with the product ID parameter.
     */
    public String productName(int productID) {
        for (Product i : this.Products.keySet()) {
            if (i.getId() == productID) {
                return i.getName();
            }
        }
        return "NULL";
    }

    /**
     * Method for getting the product price from the product ID.
     * @param productID An integer product ID.
     * @return The product price associated with the product ID parameter.
     */
    public double productPrice(int productID) {
        for (Product i : this.Products.keySet()) {
            if (i.getId() == productID) {
                return i.getPrice();
            }
        }
        return 0;
    }

    /**
     * Method for checking if the ID of a product is true given the product ID. Returns false, if otherwise.
     * @param productID An integer product ID.
     * @return A boolean true if the given product ID matches the one in the hashmap. Returns a boolean false, if otherwise.
     */
    public boolean checkID(int productID) {
        for (Product i : this.Products.keySet()) {
            if (i.getId() == productID) {
                return true;
            }
        }
        return false;
    }
}
