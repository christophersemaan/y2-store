package mystore;

// Maximus Curkovic, 101139937
// Christopher Semaan, 101140813

/**
 * Interface ProductStockContainer that implements the Inventory and ShoppingCart classes.
 *
 * @author Maximus Curkovic, 101139937
 * @author Christopher Semaan, 101140813
 */
public interface ProductStockContainer {

    /**
     * Method for getting the product quantity.
     * @param product A Product object product.
     * @return An integer product quantity.
     */
    int getProductQuantity(Product product);

    /**
     * Method for adding the product quantity.
     * @param product A Product object product.
     * @param quantity An integer quantity.
     */
    void addProductQuantity(Product product, int quantity);

    /**
     * Method for removing the product quantity.
     * @param product A Product object product.
     * @param quantity An integer quantity.
     */
    void removeProductQuantity(Product product, int quantity);

    /**
     * Method for getting the number of products.
     * @return An integer number of products.
     */
    int getNumOfProducts();

}
