package mystore;

// Maximus Curkovic, 101139937
// Christopher Semaan, 101140813

import java.util.HashMap;

/**
 * Class for the store ShoppingCart of the user.
 *
 * @author Maximus Curkovic, 101139937
 * @author Christopher Semaan, 101140813
 */
public class ShoppingCart implements ProductStockContainer{

    private HashMap<Product, Integer> Products;

    /**
     * Default constructor for the ShoppingCart class.
     */
    public ShoppingCart(){
        this.Products = new HashMap<Product, Integer>();
    }

    /**
     * Getter method for getting the Products hashmap.
     * @return A Product object, Integer hashmap Products.
     */
    public HashMap<Product, Integer> getProducts() {
        return Products;
    }

    /**
     * Getter method for getting the size of the Products hashmap.
     * @return An integer size of Products.
     */
    @Override
    public int getNumOfProducts() {
        return Products.size();
    }

    /**
     * Getter method for getting the stock of the inventory.
     * @param product A Product object product.
     * @return The stock of the product in the inventory. Will return 0 if the product does not exist.
     */
    public int getProductQuantity(Product product) {
        for (Product i : this.Products.keySet()) {
            if (i.getId() == product.getId()) {
                return this.Products.get(i);
            }
        }
        return 0;
    }
    /**
     * Method for adding items to the shopping cart, using the product.
     * @param product A Product object product.
     * @param stock An integer stock.
     */
    public void addProductQuantity(Product product, int stock) {
        if (this.Products.containsKey(product)){
            for (Product i: this.Products.keySet()) {
                if (product == i) {
                    this.Products.replace(product, this.Products.get(product),this.Products.get(product) + stock);
                }
            }
        }
        else {
            this.Products.put(product, stock);

        }

    }

    /**
     * Method for removing items from the shopping cart, using the product ID.
     * @param product A Product object product.
     * @param item An integer item.
     */
    public void removeProductQuantity(Product product, int item) {

        for (Product i : this.Products.keySet()) {
            if (i.getId() == product.getId()) {
                this.Products.replace(i, this.Products.get(i),this.Products.get(i) - item);
                if (this.Products.get(i) < 0) {
                    this.Products.remove(i);
                }
            }
        }
    }

}
