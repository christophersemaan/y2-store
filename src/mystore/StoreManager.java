package mystore;

// Maximus Curkovic, 101139937
// Christopher Semaan, 101140813

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class StoreManager for managing the store.
 *
 * @author Maximus Curkovic, 101139937
 * @author Christopher Semaan, 101140813
 */
public class StoreManager {

    private Inventory inventory;
    private HashMap<Integer, ShoppingCart> users;


    /**
     * Default constructor for the StoreManager class.
     */
    public StoreManager() {
        this.inventory = new Inventory();
        this.users = new HashMap<>();
        addToInventory(new Product("Cristina Ruiz Martin", 1, 2.99), 2);
        addToInventory(new Product("Cheryl Schramm", 2, 3.99), 1051);
        addToInventory(new Product("Lynn Marshall", 3, 4.99), 2006);
        addToInventory(new Product("Ramy Gohary", 4, 5.99), 2320);

    }

    /**
     * Method for adding a product of an integer quantity to the inventory.
     * @param product A Product object product.
     * @param quantity An integer quantity.
     */
    public void addToInventory(Product product, int quantity){
        this.inventory.addProductQuantity(product, quantity);
    }

    /**
     * Getter method for getting the inventory of the store.
     * @return An store.Inventory object inventory.
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Method for returning the inventory of Products as an ArrayList.
     * @return An ArrayList of Product objects.
     */
    public ArrayList<Product> getProductsAsArrayList(){
        return this.inventory.getProductsAsArrayList();
    }

    /**
     * Getter method for getting the users.
     * @return An Integer, ShoppingCart object hashmap users.
     */
    public HashMap<Integer, ShoppingCart> getUsers() {
        return users;
    }

    /**
     * Method for checking the inventory stock of a product, given the product ID.
     * @param product A Product object product.
     * @return The stock of the product associated with the product ID, as an integer.
     */
    public int checkStock(Product product) {
        return this.inventory.getProductQuantity(product);
    }

    public HashMap<Product, Integer> getProducts() {
        return this.inventory.getProducts();
    }
    /**
     * Method for processing the transaction of the user's shopping cart.
     * @param cartID An integer cartID.
     * @return The total price of the transaction as a double value.
     */
    public double transaction(int cartID){
        double total = 0;
        for (int i: users.keySet()){
            if(i==cartID){
                for (Product j: this.users.get(cartID).getProducts().keySet()){
                    total += this.users.get(cartID).getProducts().get(j) * j.getPrice();
                }
                return Math.round(total * 100.0) / 100.0;
            }
        }
        return total;
    }

    /**
     * Method for adding a product and an amount of the product to a user's shopping cart.
     * @param product A Product object product.
     * @param count An integer count.
     * @param cartID An integer cartID.
     */
    public void addItem(Product product, int count, int cartID){
        this.users.get(cartID).addProductQuantity(product, count);
        this.inventory.removeProductQuantity(product, count);
    }

    /**
     * Method for removing a product and an amount of the product to a user's shopping cart.
     * @param product A Product object product.
     * @param count An integer count.
     */
    public void removeItem(Product product, int count, int cartID){
        this.users.get(cartID).removeProductQuantity(product, count);
        this.inventory.addProductQuantity(product, count);
    }

    /**
     * Method for assigning a new cart ID to a user.
     * @return An integer newID for the user's shopping cart.
     */
    public int assignNewCartID(){
        int newID = this.users.size();
        this.users.put(newID, new ShoppingCart());
        return newID;
    }

}
