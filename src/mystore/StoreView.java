package mystore;

// Maximus Curkovic, 101139937
// Christopher Semaan, 101140813

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.*;
import java.awt.*;

/**
 * Class StoreView for displaying the store interface.
 *
 * @author Maximus Curkovic, 101139937
 * @author Christopher Semaan, 101140813
 */
public class StoreView {

    private StoreManager storeManager;
    private int id;
    private final JFrame frame;
    private final JPanel headerPanel, bodyPanel, endPanel;
    private final JLabel headerLabel;
    private final JButton viewCart, checkOut, quit;
    private final JPanel[] productPanels;
    private final ArrayList<JButton> add;
    private final ArrayList<JButton> remove;
    private final ArrayList<JPanel> pictures;
    private final JLabel[] priceTags;
    private final JLabel[] imageLabels;

    /**
     * Default constructor for StoreView. Defines all of the necessary attributes, including Swing objects, for the store interface.
     * @param storeManager A StoreManager object storeManager.
     * @param id An integer cart id.
     */
    public StoreView(StoreManager storeManager, int id){
        this.storeManager = storeManager;
        this.id = id;
        this.frame = new JFrame();
        this.headerLabel = new JLabel("Welcome to the Store Store! Cart ID: " + id);
        this.headerPanel = new JPanel(new BorderLayout());
        this.bodyPanel = new JPanel(new GridLayout(2,2));
        this.endPanel = new JPanel(new BorderLayout());
        this.viewCart = new JButton("View Cart");
        this.checkOut = new JButton("Checkout");
        this.quit = new JButton("Quit");
        this.add = new ArrayList<>(4);
        for (int i = 0; i < 4; ++i){
            this.add.add(new JButton("+"));
        }
        this.remove = new ArrayList<>(4);
        for (int i = 0; i < 4; ++i){
            this.remove.add(new JButton("-"));
        }
        this.productPanels = new JPanel[4];
        for (int i = 0; i < 4; ++i){
            this.productPanels[i] = new JPanel(new BorderLayout());
        }
        this.pictures = new ArrayList<>(4);
        for (int i = 0; i < 4; ++i){
            this.pictures.add(new JPanel(new BorderLayout()));
        }
        this.priceTags = new JLabel[4];
        for (int i = 0; i < 4; ++i){
            this.priceTags[i] = new JLabel();
        }
        this.imageLabels = new JLabel[4];
        Image cheryl = new ImageIcon(this.getClass().getResource("cheryl.png")).getImage();
        this.imageLabels[0] = new JLabel(new ImageIcon(cheryl));

        Image ramy = new ImageIcon(this.getClass().getResource("ramy.png")).getImage();
        this.imageLabels[1] = new JLabel(new ImageIcon(ramy));

        Image cristina = new ImageIcon(this.getClass().getResource("cristina.jpg")).getImage();
        this.imageLabels[2] = new JLabel(new ImageIcon(cristina));

        Image lynn = new ImageIcon(this.getClass().getResource("lynn.png")).getImage();
        this.imageLabels[3] = new JLabel(new ImageIcon(lynn));


    }

    /**
     * Getter method for getting the shopper's cart id.
     * @return An integer cart id.
     */
    public int getID(){
        return this.id;
    }


    /**
     * Void method for displaying The Store Store GUI.
     * Defines all of the attributes associated with developing the GUI.
     * The user can add or remove items from their cart, view their cart, quit the store interface, or checkout.
     */
    public void displayGUI(){
        frame.setTitle("The Store Store");
        headerPanel.setPreferredSize(new Dimension(600, 100));
        bodyPanel.setPreferredSize(new Dimension(600, 200));
        endPanel.setPreferredSize(new Dimension(600, 100));
        viewCart.setPreferredSize(new Dimension(200, 100));
        checkOut.setPreferredSize(new Dimension(200, 100));
        quit.setPreferredSize(new Dimension(200, 100));
        headerPanel.add(headerLabel);
        headerLabel.setHorizontalAlignment(SwingConstants.CENTER);
        headerLabel.setVerticalAlignment(SwingConstants.CENTER);
        for (int i = 0; i < 4; ++i){
            bodyPanel.add(productPanels[i]);
            productPanels[i].add(add.get(i), BorderLayout.EAST);
            productPanels[i].add(remove.get(i), BorderLayout.WEST);
            productPanels[i].add(priceTags[i], BorderLayout.SOUTH);

        }
        for (int j = 0; j < 4; ++j){
            Product p = this.storeManager.getProductsAsArrayList().get(j);
            priceTags[j].setText(("Stock: " + this.storeManager.getProducts().get(p) + " | " + p.getName() + " | $" + p.getPrice()));
        }

        this.imageLabels[0].setPreferredSize(new Dimension(10,10));
        productPanels[0].add(this.imageLabels[0], BorderLayout.CENTER);
        productPanels[1].add(this.imageLabels[1], BorderLayout.CENTER);
        productPanels[2].add(this.imageLabels[2], BorderLayout.CENTER);
        productPanels[3].add(this.imageLabels[3], BorderLayout.CENTER);
        frame.add(headerPanel, BorderLayout.PAGE_START);
        frame.add(bodyPanel, BorderLayout.CENTER);
        frame.add(endPanel, BorderLayout.PAGE_END);
        endPanel.add(viewCart, BorderLayout.EAST);
        endPanel.add(checkOut, BorderLayout.CENTER);
        endPanel.add(quit, BorderLayout.WEST);

        add.get(0).addActionListener(new ActionListener() {
            @Override
            /**
             * An action listener for adding the first product to the user's cart.
             * Fails if the inventory stock is greater than or equal to 0.
             */
            public void actionPerformed(ActionEvent ae) {
                Product key = (Product) storeManager.getProducts().keySet().toArray()[0];
                Integer value = (Integer) storeManager.getProducts().values().toArray()[0];
                if (value > 0){
                    storeManager.addItem(key, 1, id);
                    value = (Integer) storeManager.getProducts().values().toArray()[0];
                    priceTags[0].setText(("Stock: " + value + " | " + key.getName() + " | $" + key.getPrice()));
                }
            }
        });

        add.get(1).addActionListener(new ActionListener() {
            @Override
            /**
             * An action listener for adding the second product to the user's cart.
             * Fails if the inventory stock is greater than or equal to 0.
             */
            public void actionPerformed(ActionEvent ae) {
                Product key = (Product) storeManager.getProducts().keySet().toArray()[1];
                Integer value = (Integer) storeManager.getProducts().values().toArray()[1];
                if (value > 0){
                    storeManager.addItem(key, 1, id);
                    value = (Integer) storeManager.getProducts().values().toArray()[1];
                    priceTags[1].setText(("Stock: " + value + " | " + key.getName() + " | $" + key.getPrice()));
                }
            }
        });

        add.get(2).addActionListener(new ActionListener() {
            @Override
            /**
             * An action listener for adding the third product to the user's cart.
             * Fails if the inventory stock is greater than or equal to 0.
             */
            public void actionPerformed(ActionEvent ae) {
                Product key = (Product) storeManager.getProducts().keySet().toArray()[2];
                Integer value = (Integer) storeManager.getProducts().values().toArray()[2];
                if (value > 0){
                    storeManager.addItem(key, 1, id);
                    value = (Integer) storeManager.getProducts().values().toArray()[2];
                    priceTags[2].setText(("Stock: " + value + " | " + key.getName() + " | $" + key.getPrice()));
                }
            }
        });

        add.get(3).addActionListener(new ActionListener() {
            @Override
            /**
             * An action listener for adding the fourth product to the user's cart.
             * Fails if the inventory stock is greater than or equal to 0.
             */
            public void actionPerformed(ActionEvent ae) {
                Product key = (Product) storeManager.getProducts().keySet().toArray()[3];
                Integer value = (Integer) storeManager.getProducts().values().toArray()[3];
                if (value > 0){
                    storeManager.addItem(key, 1, id);
                    value = (Integer) storeManager.getProducts().values().toArray()[3];
                    priceTags[3].setText(("Stock: " + value + " | " + key.getName() + " | $" + key.getPrice()));
                }
            }
        });

        remove.get(0).addActionListener(new ActionListener() {
            @Override
            /**
             * An action listener for removing the first product from the user's cart.
             * Fails if the cart stock is greater than 0.
             */
            public void actionPerformed(ActionEvent ae) {
                Integer cartVal;
                Product key = (Product) storeManager.getProducts().keySet().toArray()[0];
                Integer inventoryVal;
                for(Product p: storeManager.getUsers().get(id).getProducts().keySet()) {
                    if (p == key){
                        cartVal = storeManager.getUsers().get(id).getProducts().get(p);
                        if (cartVal > 0){
                            storeManager.removeItem(key, 1, id);
                            inventoryVal = (Integer) storeManager.getProducts().values().toArray()[0];
                            priceTags[0].setText(("Stock: " + inventoryVal + " | " + key.getName() + " | $" + key.getPrice()));
                        }
                    }
                }
            }
        });

        remove.get(1).addActionListener(new ActionListener() {
            @Override
            /**
             * An action listener for removing the second product from the user's cart.
             * Fails if the cart stock is greater than 0.
             */
            public void actionPerformed(ActionEvent ae) {
                Integer cartVal1;
                Product key1 = (Product) storeManager.getProducts().keySet().toArray()[1];
                Integer inventoryVal1;
                for(Product p: storeManager.getUsers().get(id).getProducts().keySet()) {
                    if (p == key1){
                        cartVal1 = storeManager.getUsers().get(id).getProducts().get(p);
                        if (cartVal1 > 0){
                            storeManager.removeItem(key1, 1, id);
                            inventoryVal1 = (Integer) storeManager.getProducts().values().toArray()[1];
                            priceTags[1].setText(("Stock: " + inventoryVal1 + " | " + key1.getName() + " | $" + key1.getPrice()));
                        }
                    }
                }
            }
        });

        remove.get(2).addActionListener(new ActionListener() {
            @Override
            /**
             * An action listener for removing the third product from the user's cart.
             * Fails if the cart stock is greater than 0.
             */
            public void actionPerformed(ActionEvent ae) {
                Integer cartVal2;
                Product key2 = (Product) storeManager.getProducts().keySet().toArray()[2];
                Integer inventoryVal2;
                for(Product p: storeManager.getUsers().get(id).getProducts().keySet()) {
                    if (p == key2){
                        cartVal2 = storeManager.getUsers().get(id).getProducts().get(p);
                        if (cartVal2 > 0){
                            storeManager.removeItem(key2, 1, id);
                            inventoryVal2 = (Integer) storeManager.getProducts().values().toArray()[2];
                            priceTags[2].setText(("Stock: " + inventoryVal2 + " | " + key2.getName() + " | $" + key2.getPrice()));
                        }
                    }
                }
            }
        });

        remove.get(3).addActionListener(new ActionListener() {
            @Override
            /**
             * An action listener for removing the fourth product from the user's cart.
             * Fails if the cart stock is greater than 0.
             */
            public void actionPerformed(ActionEvent ae) {
                Integer cartVal3;
                Product key3 = (Product) storeManager.getProducts().keySet().toArray()[3];
                Integer inventoryVal3;
                for(Product p: storeManager.getUsers().get(id).getProducts().keySet()) {
                    if (p == key3){
                        cartVal3 = storeManager.getUsers().get(id).getProducts().get(p);
                        if (cartVal3 > 0){
                            storeManager.removeItem(key3, 1, id);
                            inventoryVal3 = (Integer) storeManager.getProducts().values().toArray()[3];
                            priceTags[3].setText(("Stock: " + inventoryVal3 + " | " + key3.getName() + " | $" + key3.getPrice()));
                        }
                    }
                }
            }
        });
        quit.addActionListener(new ActionListener() {
            @Override
            /**
             * An action listener for quitting the GUI interface.
             */
            public void actionPerformed(ActionEvent ae) {
                if (JOptionPane.showConfirmDialog(frame, "Are you sure you want to quit?")
                        == JOptionPane.OK_OPTION) {
                    frame.setVisible(false);
                    frame.dispose();
                }
            }
        });

        viewCart.addActionListener(new ActionListener() {
            @Override
            /**
             * An action listener for viewing the user's cart.
             * Will prompt an alternate message pane if the user's cart is empty.
             */
            public void actionPerformed(ActionEvent ae) {
                if (storeManager.getUsers().get(id).getProducts().isEmpty()){
                    storeManager.getUsers().get(id).getProducts().clear();
                    JOptionPane.showMessageDialog(frame, "Your cart is empty!");
                }
                else {
                    String viewCart = "Contents in your cart:\n";
                    for (Product j : storeManager.getUsers().get(id).getProducts().keySet()) {
                        if (storeManager.getUsers().get(id).getProducts().get(j) == 0){
                            JOptionPane.showMessageDialog(frame, "Your cart is empty!");
                            storeManager.getUsers().get(id).getProducts().clear();
                            break;
                        }
                        String product = String.format(("Item: " + j.getName() + ", Quantity: " + storeManager.getUsers().get(id).getProducts().get(j) + " -> $" + j.getPrice() + " each\n"));
                        viewCart = viewCart + product;
                    }
                    if (storeManager.getUsers().get(id).getProducts().size() != 0){
                        JOptionPane.showMessageDialog(frame, viewCart + "Your current total is: $" + storeManager.transaction(id), "Your Shopping Cart (User ID: " + id + ")", JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            }
        });

         checkOut.addActionListener(new ActionListener() {
            @Override
            /**
             * An action listener for checking out with the items currently in the user's cart.
             * Will prompt an alternate message pane if the user's cart is empty.
             */
            public void actionPerformed(ActionEvent ae) {
                String checkout = "Contents in your cart:\n";
                if (storeManager.getUsers().get(id).getProducts().isEmpty()){
                    JOptionPane.showMessageDialog(frame, "Your cart is empty!");
                }
                else {
                    for (Product j : storeManager.getUsers().get(id).getProducts().keySet()) {
                        String product = String.format(("Item: " + j.getName() + ", Quantity: " + storeManager.getUsers().get(id).getProducts().get(j) + " -> $" + j.getPrice() + " each\n"));
                        checkout = checkout + product;
                    }
                    JOptionPane.showMessageDialog(frame, checkout + "Your total today is: $" + storeManager.transaction(id) + "\n\n Have a heavenly day!", "Your Shopping Cart Total (User ID: " + id + ")", JOptionPane.INFORMATION_MESSAGE);
                    storeManager.getUsers().get(id).getProducts().clear();
                    frame.setVisible(false);
                    frame.dispose();
                }
            }
        });

        frame.addWindowListener(new WindowAdapter() {
            @Override
            /**
             * Window listener method for prompting a message if the user is sure that they want to quit the program.
             */
            public void windowClosing(WindowEvent we) {
                if (JOptionPane.showConfirmDialog(frame, "Are you sure you want to quit?")
                        == JOptionPane.OK_OPTION) {
                    frame.setVisible(false);
                    frame.dispose();
                }
            }
        });

        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Main method for calling the displayGUI method.
     * @param args A string array args.
     */
    public static void main(String[] args) {
        StoreManager sm = new StoreManager();
        StoreView sv1 = new StoreView(sm, sm.assignNewCartID());
        sv1.displayGUI();
    }
}
