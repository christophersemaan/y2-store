package mystore;// Maximus Curkovic, 101139937
// Christopher Semaan, 101140813


// The entire class was written by Christopher Semaan.

/**
 * A class store.Product that defines a product object with a string name, int id, and double price.
 * @author Maximus Curkovic, 101139937
 * @author Christopher Semaan, 101140813
 */
public class Product {
    private final String name;
    private final int id;
    private final double price;

    /**
     * Default constructor for a store.Product class.
     */
    public Product(String name, int id, double price){
        this.name = name;
        this.id = id;
        this.price = price;
    }

    /**
     * Getter method to return the name of the product.
     * @return A String name.
     */
    public String getName() { return name;}

    /**
     * Getter method to return the price of the product.
     * @return A double price.
     */
    public double getPrice() { return price; }

    /**
     * Getter method to return the ID of the product.
     * @return An Integer id.
     */
    public int getId() {
        return id;
    }
}

